from typing import Any, Dict


def apply(hub, event: Dict, ref: str) -> Dict[str, Any]:
    ...
