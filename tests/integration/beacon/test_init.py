from typing import AsyncGenerator

import mock
import pytest


def test_cli(hub):
    with mock.patch("sys.argv", ["beacon"]):
        # Stop everything before it starts
        hub.beacon.RUN_FOREVER = False
        hub.evbus.RUN_FOREVER = False

        hub.pop.Loop.run_until_complete(hub.evbus.init.stop())
        hub.pop.Loop.run_until_complete(hub.beacon.local.stop())

        # Run the cli, it should return immediately
        hub.beacon.init.cli()


@pytest.mark.asyncio
async def test_start(hub):
    hub.beacon.RUN_FOREVER = False
    hub.evbus.RUN_FOREVER = False
    await hub.beacon.init.start(format_plugin="raw", sub_profiles={})


@pytest.mark.asyncio
async def test_listeners(hub):
    listeners = hub.beacon.init.listeners({})
    assert all(isinstance(listen, AsyncGenerator) for listen in listeners)


@pytest.mark.asyncio
async def test_listen(hub):
    hub.beacon.RUN_FOREVER = False
    async for event, ref in hub.beacon.init.listen():
        assert ref == "beacon.init"
        assert event is hub.beacon.init.STOP_ITERATION
